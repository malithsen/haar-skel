Based on  Naotoshi Seo's [article](http://note.sonots.com/SciSoftware/haartraining.html)

Step 1: Put all the positive images in positives directory and run (in the parent directory)
```
find ./positives -iname "*.jpg" > positives.txt
```

Step 2: Put the negative images in negatives directory and run
```
find ./negatives -iname "*.jpg" > negatives.txt
```

Step 3: Create the samples
```
 perl bin/createsamples.pl positives.txt negatives.txt samples 1500\
  "opencv_createsamples -bgcolor 0 -bgthresh 0 -maxxangle 1.1\
  -maxyangle 1.1 maxzangle 0.5 -maxidev 40 -w 20 -h 20"
```

Step 4: Compile mergevec
```
cp src/mergevec.cpp ~/opencv-2.4.9/apps/haartraining
cd ~/opencv-2.4.9/apps/haartraining
g++ `pkg-config --libs --cflags opencv | sed 's/libtbb\.dylib/tbb/'`\
  -I. -o mergevec mergevec.cpp\
  cvboost.cpp cvcommon.cpp cvsamples.cpp cvhaarclassifier.cpp\
  cvhaartraining.cpp\
  -lopencv_core -lopencv_calib3d -lopencv_imgproc -lopencv_highgui -lopencv_objdetect
```

Step 5: Merge the samples
```
find ./samples -name '*.vec' > samples.txt
./mergevec samples.txt samples.vec
```

Step 6: Start training
```
opencv_traincascade -data classifier -vec samples.vec -bg negatives.txt\
  -numStages 20 -minHitRate 0.999 -maxFalseAlarmRate 0.5 -numPos 1000\
  -numNeg 600 -w 20 -h 20 -mode ALL
```

There's an equation to get the number of negative images you got to have for a given number of positive images. Can't find it's source.


If you want to generate the xml at an intermediate state, rerun opencv_traincascade with -numStages one less than the last intermediate xml generated.

Link to my blog [post](http://malithsen.blogspot.com/2015/05/how-to-train-your-haar-classifier-ive.html)

